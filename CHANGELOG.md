# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/tezos-domains/dns-verify-api/compare/v1.0.0...v1.1.0) (2024-05-02)


### Features

* upgrade packages ([995a980](https://gitlab.com/tezos-domains/dns-verify-api/commit/995a9806a6de43da652f9a2e2f760f880d2078c6))

## 1.0.0 (2022-05-24)


### Features

* add azure remote signing instead of InMemorySigner ([a0d97af](https://gitlab.com/tezos-domains/dns-verify-api/commit/a0d97af398007dbe7c35ed048cad8afbe486f85c))
* add staging mock implementation for DNS check ([45f4c9e](https://gitlab.com/tezos-domains/dns-verify-api/commit/45f4c9e984bc22903b43fcffeedba4f6ea38e8ee))
* domain validation ([a88c260](https://gitlab.com/tezos-domains/dns-verify-api/commit/a88c260896a2c67319b1131464c1c09aaefdefcf))
* parse body data, add validation ([7735377](https://gitlab.com/tezos-domains/dns-verify-api/commit/773537725b7adf3f68a1910159ca792f83308053))
* use InMemorySigner and update CI ([3834833](https://gitlab.com/tezos-domains/dns-verify-api/commit/383483322e94ecc904935d6de546ce9d223de886))


### Bug Fixes

* dnsprovejs requires typescript-logging ([4737cc5](https://gitlab.com/tezos-domains/dns-verify-api/commit/4737cc536a72f62bba21d13eb547bde97d3a6384))
* initialize appInsigts with connection string ([beb7603](https://gitlab.com/tezos-domains/dns-verify-api/commit/beb7603b00f37b76e2aee33fad9de1b0cc9e4114))
* return exception ([1e003e5](https://gitlab.com/tezos-domains/dns-verify-api/commit/1e003e52c647348ee5f88f9abf4e557a7ae0147e))
* test ([9ac004d](https://gitlab.com/tezos-domains/dns-verify-api/commit/9ac004d7171dd07bf4fe91d9b85b45bdfa02da64))
* try blake3 ([43029aa](https://gitlab.com/tezos-domains/dns-verify-api/commit/43029aadd2f9c9c7684b83c4c6f3f95c10240189))
* try stablelib/blake2b ([72f4b21](https://gitlab.com/tezos-domains/dns-verify-api/commit/72f4b2151bb077699aa98e206b3280022b1a42c2))
