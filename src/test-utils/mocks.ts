/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Provider } from '@nestjs/common';
import { instance } from 'ts-mockito';

export function provider(mock: any, type?: any): Provider {
    type = type || mock.clazz.mocker.clazz;
    return { provide: type, useFactory: () => instance(mock) };
}

export function providers(...mocks: any[]): Provider[] {
    return mocks.map(mock => {
        const type = mock.clazz.mocker.clazz;
        return { provide: type, useFactory: () => instance(mock) };
    });
}
