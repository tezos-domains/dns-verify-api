import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as appInsights from 'applicationinsights';
import { AppModule } from './app.module';
import { Configuration } from './configuration';
import { GenericExceptionFilter } from './filters/global-exception.filter';

appInsights.setup(process.env.APPLICATIONINSIGHTS_CONNECTION_STRING).start();

export async function createApp(): Promise<INestApplication> {
    const app = await NestFactory.create(AppModule);

    app.setGlobalPrefix('api');
    app.useGlobalFilters(new GenericExceptionFilter(app.get(Configuration)));
    app.useGlobalPipes(new ValidationPipe({ validationError: { target: true } }));

    await app.init();
    return app;
}
