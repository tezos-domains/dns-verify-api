import { DNSCheckServiceImpl } from './dns-check-impl.service';
import { DNSCheckService } from './dns-check.service';
import { DomainValidationRequest, DomainValidationResponse } from '../models';

export class DNSCheckServiceFake extends DNSCheckService {
    constructor(private dnsCheckService: DNSCheckServiceImpl) {
        super();
    }

    async validate(data: DomainValidationRequest): Promise<DomainValidationResponse> {
        if (data.label.startsWith('dns-')) {
            return Promise.resolve(<DomainValidationResponse>{
                status: 'NoDNSSEC',
            });
        }

        if (data.label.startsWith('txt-m-')) {
            return Promise.resolve(<DomainValidationResponse>{
                status: 'MultipleAddressesInTXT',
            });
        }

        if (data.label.startsWith('txt-a-')) {
            return Promise.resolve(<DomainValidationResponse>{
                status: 'NoAddressInTXT',
            });
        }

        if (data.label.startsWith('txt-no-')) {
            return Promise.resolve(<DomainValidationResponse>{
                status: 'TXTNoMatch',
                txtValue: 'tz1RVqScMr62n1sN1XYRC9hZHK5Qc8ayQYF6',
            });
        }

        if (data.label.startsWith('txt-')) {
            return Promise.resolve(<DomainValidationResponse>{
                status: 'NoTXT',
            });
        }

        if (data.label.startsWith('ok-')) {
            return Promise.resolve(<DomainValidationResponse>{
                status: 'OK',
                txtValue: data.owner,
            });
        }

        return this.dnsCheckService.validate(data);
    }
}
