import { NoValidDnskeyError, NoValidDsError, ProvableAnswer, ResponseCodeError } from '@ensdomains/dnsprovejs';
import { TxtAnswer } from 'dns-packet';
import { Configuration } from '../configuration';
import { DomainValidationRequest, DomainValidationResponse } from '../models';
import { isNotNullish } from '../utils/reflection';
import { serializeError } from '../utils/serialize-error';
import { DNSCheckService } from './dns-check.service';
import { DnsProverService } from './prover.service';

// The TXT record that proves ownership of the domain should look like `_tda=tz1xxx...`;
const TXTAddressPrefix = '_tda';

export class DNSCheckServiceImpl extends DNSCheckService {
    constructor(
        private config: Configuration,
        private dnsProver: DnsProverService
    ) {
        super();
    }

    async validate(data: DomainValidationRequest): Promise<DomainValidationResponse> {
        const domain = `${data.label}.${data.tld}`;

        let dnsResult: ProvableAnswer<TxtAnswer> | null = null;

        try {
            dnsResult = await this.dnsProver.queryTXT(domain);
        } catch (ex) {
            if (ex instanceof ResponseCodeError) {
                // eslint-disable-next-line @typescript-eslint/no-explicit-any
                return { status: 'NoDNSSEC', dnsErrorMessage: serializeError(ex, this.config.isStaging), dnsErrorCode: (<any>ex.response)?.rcode };
            }

            if (ex instanceof NoValidDnskeyError || ex instanceof NoValidDsError) {
                return { status: 'FailedDNSSEC', dnsErrorMessage: serializeError(ex, this.config.isStaging) };
            }

            throw ex;
        }

        if (!dnsResult) {
            return { status: 'NoTXT' };
        }

        const answersWithAddresses =
            dnsResult.answer?.records
                .filter(isNotNullish)
                .map(r => r.data.toString())
                .filter(r => r.includes(TXTAddressPrefix)) || [];

        if (!answersWithAddresses.length) {
            return { status: 'NoAddressInTXT' };
        }

        if (answersWithAddresses.length !== 1) {
            return { status: 'MultipleAddressesInTXT' };
        }

        const txtValue = answersWithAddresses[0];

        if (txtValue) {
            const txtAddress = txtValue.trim().replace(`${TXTAddressPrefix}=`, '');
            if (txtAddress === data.owner) {
                return { status: 'OK', txtValue: txtAddress };
            }

            return { status: 'TXTNoMatch', txtValue: txtAddress };
        }

        return { status: 'NoTXT' };
    }
}
