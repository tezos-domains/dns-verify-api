import { NoValidDnskeyError, NoValidDsError, ProvableAnswer, ResponseCodeError, SignedSet } from '@ensdomains/dnsprovejs';
import { TxtAnswer } from 'dns-packet';
import { anyString, instance, mock, when } from 'ts-mockito';
import { TestConfig } from '../test-utils/data';
import { DNSCheckServiceImpl } from './dns-check-impl.service';
import { DnsProverService } from './prover.service';

describe('DnsCheckServiceImpl', () => {
    let service: DNSCheckServiceImpl;
    let dnsProver: DnsProverService;

    beforeEach(async () => {
        dnsProver = mock(DnsProverService);
        service = new DNSCheckServiceImpl(TestConfig, instance(dnsProver));
    });

    it('should handle ResponseCodeError', async () => {
        when(dnsProver.queryTXT(anyString())).thenReject(Object.create(ResponseCodeError.prototype));

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('NoDNSSEC');
    });

    it('should handle NoValidDnskeyError', async () => {
        when(dnsProver.queryTXT(anyString())).thenReject(Object.create(NoValidDnskeyError.prototype));

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('FailedDNSSEC');
    });

    it('should handle NoValidDsError', async () => {
        when(dnsProver.queryTXT(anyString())).thenReject(Object.create(NoValidDsError.prototype));

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('FailedDNSSEC');
    });

    it('should handle null response from DnsProver', async () => {
        when(dnsProver.queryTXT(anyString())).thenResolve(null);

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('NoTXT');
    });

    it('should handle no TXT record with correct prefix', async () => {
        when(dnsProver.queryTXT(anyString())).thenResolve(buildTXTAnswers('blueblue'));

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('NoAddressInTXT');
    });

    it('should handle multiple TXT record with correct prefix', async () => {
        when(dnsProver.queryTXT(anyString())).thenResolve(buildTXTAnswers('_tda=1234', '_tda=4321'));

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('MultipleAddressesInTXT');
    });

    it('should return TXTNoMatch if owner doesn`t match', async () => {
        when(dnsProver.queryTXT(anyString())).thenResolve(buildTXTAnswers('_tda=tz1xxxy'));

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('TXTNoMatch');
        expect(result.txtValue).toBe('tz1xxxy');
    });

    it('should return OK if owner matches', async () => {
        when(dnsProver.queryTXT(anyString())).thenResolve(buildTXTAnswers('_tda=tz1xxx'));

        const result = await service.validate({ label: 'blue', tld: 'com', owner: 'tz1xxx' });
        expect(result.status).toBe('OK');
        expect(result.txtValue).toBe('tz1xxx');
    });
});

function buildTXTAnswers(...values: string[]): ProvableAnswer<TxtAnswer> {
    return <ProvableAnswer<TxtAnswer>>{
        answer: { records: values.map(v => ({ data: Buffer.from(v) })) } as SignedSet<TxtAnswer>,
    };
}
