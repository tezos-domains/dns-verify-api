import { DomainValidationRequest, DomainValidationResponse } from '../models';

export abstract class DNSCheckService {
    abstract validate(data: DomainValidationRequest): Promise<DomainValidationResponse>;
}
