import { DNSProver, ProvableAnswer } from '@ensdomains/dnsprovejs';
import { Injectable } from '@nestjs/common';
import { TxtAnswer } from 'dns-packet';

@Injectable()
export class DnsProverService {
    async queryTXT(domain: string): Promise<ProvableAnswer<TxtAnswer> | null> {
        const prover = DNSProver.create('https://cloudflare-dns.com/dns-query');
        return prover.queryWithProof('TXT', domain) as Promise<ProvableAnswer<TxtAnswer> | null>;
    }
}
