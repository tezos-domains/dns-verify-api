import { Module } from '@nestjs/common';
import { strict as assert } from 'assert';
import { AppController } from './app.controller';
import { Configuration, Environment } from './configuration';
import { DNSCheckServiceFake } from './dns/dns-check-fake.service';
import { DNSCheckServiceImpl } from './dns/dns-check-impl.service';
import { DNSCheckService } from './dns/dns-check.service';
import { DnsProverService } from './dns/prover.service';
import { AzureSignerService } from './signature/azure-signer.service';
import { ClaimRequestGenerator } from './signature/claim-request-generator.service';
import { SignerService } from './signature/signer.service';

const configurationFactory = {
    provide: Configuration,
    useFactory: () => {
        assert(process.env.AZURE_KEYVAULT_URL, 'Azure KeyVault URL is missing (AZURE_KEYVAULT_URL)');
        assert(process.env.AZURE_KEY_NAME, 'The name of the key from Azure KeyVault is missing (AZURE_KEY_NAME)');

        return new Configuration(parseEnvironmentName(process.env.ENV_NAME), process.env.AZURE_KEYVAULT_URL, process.env.AZURE_KEY_NAME);
    },
};
const dnsCheckFactory = {
    provide: DNSCheckService,
    useFactory: (config: Configuration, prover: DnsProverService) => {
        return config.isStaging ? new DNSCheckServiceFake(new DNSCheckServiceImpl(config, prover)) : new DNSCheckServiceImpl(config, prover);
    },
    inject: [Configuration, DnsProverService],
};

function parseEnvironmentName(value?: string): Environment {
    return value === 'staging' ? 'staging' : 'production';
}

@Module({
    imports: [],
    controllers: [AppController],
    providers: [configurationFactory, dnsCheckFactory, AzureSignerService, ClaimRequestGenerator, SignerService, DnsProverService],
})
export class AppModule {}
