export type Environment = 'production' | 'staging';
export class Configuration {
    constructor(
        public readonly environment: 'staging' | 'production',
        public readonly azureKeyVaultUrl: string,
        public readonly azureKeyName: string
    ) {}

    get isStaging(): boolean {
        return this.environment === 'staging';
    }
}
