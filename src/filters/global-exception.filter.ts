import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import { Configuration } from '../configuration';
import { serializeError } from '../utils/serialize-error';

@Catch()
export class GenericExceptionFilter implements ExceptionFilter {
    constructor(private config: Configuration) {}

    catch(exception: unknown, host: ArgumentsHost): void {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

        response.status(status).json({
            statusCode: status,
            error: serializeError(exception, this.config.isStaging),
            timestamp: new Date().toISOString(),
            path: request.url,
        });
    }
}
