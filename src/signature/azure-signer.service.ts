import { DefaultAzureCredential } from '@azure/identity';
import { CryptographyClient, KeyClient, SignResult } from '@azure/keyvault-keys';
import { Injectable } from '@nestjs/common';
import { Configuration } from '../configuration';

@Injectable()
export class AzureSignerService {
    constructor(private config: Configuration) {}

    async sign(hash: Buffer): Promise<SignResult> {
        const credential = new DefaultAzureCredential();

        const keysClient = new KeyClient(this.config.azureKeyVaultUrl, credential);
        const myKey = await keysClient.getKey(this.config.azureKeyName);
        const cryptoClient = new CryptographyClient(myKey, credential);

        const signed = await cryptoClient.sign('ES256K', hash);

        return signed;
    }
}
