import { SignResult } from '@azure/keyvault-keys';
import { Injectable } from '@nestjs/common';
import { BLAKE2b } from '@stablelib/blake2b';
import { Exact } from '@tezos-domains/core';
import { ClaimRequest } from '@tezos-domains/manager';
import { strict as assert } from 'assert';
import * as bs58check from 'bs58check';
import { AzureSignerService } from './azure-signer.service';
import { ClaimRequestGenerator } from './claim-request-generator.service';

enum K {
    CURVE_ORDER = '0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141',
    CURVE_HALF_ORDER = '0x7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5D576E7357A4501DDFE92F46681B20A0',
}

enum TZ {
    PRE_SIG_P256K = '0d7365133f',
}

@Injectable()
export class SignerService {
    constructor(
        private azSigner: AzureSignerService,
        private claimRequest: ClaimRequestGenerator
    ) {}

    async sign(parameters: Exact<ClaimRequest>): Promise<string> {
        const packed = this.claimRequest.packRequest(parameters);
        const hash = this.hashData(packed);
        const azSignature = await this.azSigner.sign(hash);

        return this.buildTezosSignature(azSignature);
    }

    private hashData(packedData: string): Buffer {
        const m = Buffer.from(packedData, 'hex');
        const h = new BLAKE2b(32);
        h.update(m);

        return Buffer.from(h.digest());
    }

    private async buildTezosSignature(azureSignature: SignResult): Promise<string> {
        /*
         * From: https://github.com/lattejed/tezos-azure-hsm-signer
         */

        const raw = azureSignature.result;

        /*
         * Signer must return a 64 byte buffer
         */
        assert(Buffer.isBuffer(raw) && raw.length === 64, 'Raw signature must be a 64-byte buffer');

        /*
         * P256K signatures are of the format
         * prefix + raw signature base58 encoded with a checksum
         * S values must be in the lower half of the curve's order
         */
        /*
         * Enforce small S values
         * https://github.com/bitcoin/bips/blob/master/bip-0062.mediawiki#Low_S_values_in_signatures
         */

        const pre = Buffer.from(TZ.PRE_SIG_P256K, 'hex');
        const R = raw.slice(0, 32).toString('hex');
        const S = raw.slice(32).toString('hex');

        let s = BigInt('0x' + S);
        let sig: Buffer;

        if (s > BigInt(K.CURVE_HALF_ORDER)) {
            s = BigInt(K.CURVE_ORDER) - s;
            const hs = s.toString(16).padStart(64, '0');
            assert(hs.length === 64, 'S value of signature has invalid length');
            sig = Buffer.from(R + hs, 'hex');
        } else {
            sig = raw;
        }

        return Promise.resolve(bs58check.encode(Buffer.concat([pre, sig])));
    }
}
