import { Injectable } from '@nestjs/common';
import { MichelsonType, packDataBytes } from '@taquito/michel-codec';
import { Schema } from '@taquito/michelson-encoder';
import { Exact, RpcRequestData } from '@tezos-domains/core';
import { ClaimRequest } from '@tezos-domains/manager';

const claimSchemaExpression = {
    prim: 'pair',
    args: [
        {
            annots: ['%label'],
            prim: 'bytes',
        },
        {
            annots: ['%tld'],
            prim: 'bytes',
        },
        {
            annots: ['%owner'],
            prim: 'address',
        },
        {
            annots: ['%timestamp'],
            prim: 'timestamp',
        },
    ],
};

@Injectable()
export class ClaimRequestGenerator {
    private claimSchema = new Schema(claimSchemaExpression);

    packRequest(parameters: Exact<ClaimRequest>): string {
        const encodedRequest = RpcRequestData.fromObject(ClaimRequest, parameters).encode();
        const data = this.claimSchema.Encode(encodedRequest);

        return packDataBytes(data, claimSchemaExpression as MichelsonType).bytes;
    }
}
