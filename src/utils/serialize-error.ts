// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function serializeError(err: Error | any, detailed: boolean): string {
    if (!err) {
        return '';
    }

    return detailed ? JSON.stringify(err, Object.getOwnPropertyNames(err), 2) : err.message ?? '';
}
