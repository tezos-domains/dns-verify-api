export type Nullish<T> = T | null | undefined;

export type Nullable<T> = T | null;

export function isNotNullish<TValue>(value: Nullish<TValue>): value is TValue {
    return !isNullish(value);
}

export function isNullish<TValue>(value: Nullish<TValue>): value is null | undefined {
    return value === undefined || value === null;
}
