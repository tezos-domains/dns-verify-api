import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { ClaimRequest } from '@tezos-domains/manager';
import { DNSCheckService } from './dns/dns-check.service';
import { DomainValidationRequest, DomainValidationResponse } from './models';
import { SignerService } from './signature/signer.service';

@Controller()
export class AppController {
    constructor(
        private readonly dnsCheckService: DNSCheckService,
        private readonly signer: SignerService
    ) {}

    @Post('/verify')
    @HttpCode(200)
    async validate(@Body() data: DomainValidationRequest): Promise<DomainValidationResponse> {
        const result = await this.dnsCheckService.validate(data);

        if (result.status === 'OK') {
            const request: ClaimRequest = { ...data, timestamp: new Date().toISOString() };
            const signature = await this.signer.sign(request);

            return { ...result, signedAt: request.timestamp, signature: signature };
        }

        return result;
    }
}
