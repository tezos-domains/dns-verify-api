import { Test, TestingModule } from '@nestjs/testing';
import { anything, instance, mock, when } from 'ts-mockito';
import { AppController } from './app.controller';
import { Configuration } from './configuration';
import { DNSCheckServiceImpl } from './dns/dns-check-impl.service';
import { DNSCheckService } from './dns/dns-check.service';
import { SignerService } from './signature/signer.service';
import { TestConfig } from './test-utils/data';
import { provider } from './test-utils/mocks';

describe('AppController', () => {
    let appController: AppController;
    let dnsCheckServiceMock: DNSCheckService;
    let signerMock: SignerService;
    let now: Date;

    beforeEach(async () => {
        now = new Date();
        jest.useFakeTimers();
        jest.setSystemTime(now);

        dnsCheckServiceMock = mock(DNSCheckServiceImpl);
        signerMock = mock(SignerService);

        when(dnsCheckServiceMock.validate(anything())).thenResolve({ status: 'OK', txtValue: 'tz1' });
        when(signerMock.sign(anything())).thenResolve('hash');

        const app: TestingModule = await Test.createTestingModule({
            controllers: [AppController],
            providers: [
                provider(signerMock),
                { provide: DNSCheckService, useValue: instance(dnsCheckServiceMock) },
                { provide: Configuration, useValue: TestConfig },
            ],
        }).compile();

        appController = app.get(AppController);
    });

    afterEach(() => {
        jest.useRealTimers();
    });

    it('should return signature when dns check ok', async () => {
        appController = appController;
        const result = await appController.validate({ label: 'test', owner: 'tz1', tld: '.com' });
        expect(result).toEqual({ status: 'OK', txtValue: 'tz1', signature: 'hash', signedAt: now.toISOString() });
    });
});
