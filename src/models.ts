import { IsNotEmpty } from 'class-validator';

export type ValidationResult = 'OK' | 'DomainNonExistant' | 'NoDNSSEC' | 'FailedDNSSEC' | 'NoTXT' | 'NoAddressInTXT' | 'MultipleAddressesInTXT' | 'TXTNoMatch';

export class DomainValidationRequest {
    @IsNotEmpty()
    readonly label: string;

    @IsNotEmpty()
    readonly owner: string;

    @IsNotEmpty()
    readonly tld: string;
}

export interface DomainValidationResponse {
    readonly status: ValidationResult;
    readonly signature?: string;
    readonly txtValue?: string;
    readonly signedAt?: string;
    readonly dnsErrorMessage?: string;
    readonly dnsErrorCode?: string;
}
